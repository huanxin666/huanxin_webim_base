# 环信入门项目

#### 介绍
vue3连接环信接口，适合新手入门，简单了解一下

#### 软件架构
软件架构说明:VUE-Cli脚手架新建一个项目
直接访问环信服务端接口，获取数据
显示数据

#### 安装教程

1.  安装VUE-Cli3，引用element-ui，自行参考网上教程大把
2.  npm install
3.  npm run serve
4.  localhost:8080

#### 使用说明

1.  注册一个环信用户，官网地址https://www.easemob.com/

    ![avatar](huanxin_img/huanxin_1.png)

2.  环信即时通讯云个人用户后台，添加一个应用，网址https://console.easemob.com/index

    ![avatar](huanxin_img/huanxin_3.png)

3.  点击新建的应用->操作【查看】->进入应用后台

    ![avatar](huanxin_img/huanxin_2.png)

4.  应用概览->应用详情，查看你建立的应用的：APPKey，Client ID，ClientSecret【见上图】

5.  用4提到的APPKey，Client ID，ClientSecret替换掉目录/src/api/im/imapi.js，/common/request.js，/common/token.js这些文件中对应代码

6.  使用服务端接口文档参考地址：https://docs-im.easemob.com/im/server/ready/user

    ![avatar](huanxin_img/huanxin_4.png)

7.  前端主要是引用环信写的SDK，太复杂了，对新手极不友好，文档也太复杂了，所以我没写。

8.  仅供参考，觉得不错，点个星星
#### 参与贡献

1.  我贡献
2.  你想参与的话
3.  那就点个星星
4.  谢谢

#### 一些牢骚

1.  环信提供了很复杂的文档
2.  网上很多开源的代码，越看越一头雾水
3.  我是做后端的，才刚用VUE一个月
4.  环信客服太忙了，尽量自己搞定，别打搅人家
5.  希望开源项目仅提供基础代码，别整太复杂了

#### 特技

1.  觉得有用请点个星星，非常感激请捐赠，捐赠将用于支持开源项目【主要用于支持我能买包烟抽，更有助于敲代码】
2.  谢谢