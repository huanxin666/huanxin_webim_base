//import WebIMconfig from '@/utils/WebIMConfig'
import axios from 'axios'//axios连接服务器发送请求
import {getRequest, getRequestDataStr} from '../../common/request'
import {getToken} from '../../common/token'

export async function getToken1111() {//获取token
    return axios({
        method: 'post',
        url: 'https://a1.easemob.com/1138210906051608/demo/token',//环信token接口地址，登录环信即时通讯后台查看：【数字/demo】=你自己的app应用id/应用名称
        data: JSON.stringify({//登录环信即时通讯后台，把下面的参数替换成你自己的参数
            "grant_type": "client_credentials",
            "client_id": "YXA6c_97gAWNTGWAdoe5mgs7kg",
            "client_secret": "YXA6atrb2m-9NoNvvEGL7aykY3WLpfU"
        }),
        headers: {
            'Content-Type': 'application/json'
        },
        timeout: 10000
    }).then((res)=>{
        return res;
    })
}

export async function getUser(formData) {//访问接口获取数据
    let isToken = getToken()
    let Authorization = ''
    if (isToken != '') {
        //this.axios.headers['Authorization'] = 'Bearer ' + getToken() // 让每个请求携带token 请根据实际情况自行修改
        Authorization = 'Bearer ' + isToken
    }
    return axios({
        method: 'post',
        url: 'https://a1.easemob.com/1138210906051608/demo/user',
        data: JSON.stringify(formData),
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': Authorization
        },
        timeout: 10000
    }).then((res)=>{
        return res;
    }).catch(function (error) {
        console.log(error.response,'-------');//出错可以拿到后端返回的信息
        return error.response
    });
}

export async function regUser(formData) {//注册新用户
    return getRequest({
        url: '/users',
        method: 'post',
        data:formData
    })
}

export async function getUserList(datastr) {//获取用户列表
    return getRequest({
        url: '/users?' + datastr,
        method: 'get',
        data:JSON.stringify()
    })
}

export async function delUser(usernamestr) {//删除用户
    return getRequest({
        url: '/users/' + usernamestr,
        method: 'delete',
        data:JSON.stringify()
    })
}

export async function getUsertype(usernamestr) {//获取用户属性
    return getRequest({
        url: '/metadata/user/' + usernamestr,
        method: 'get'
    })
}

export async function editUsertype(usernamestr,usertype) {//编辑用户属性
    let formdata = {
        nickname: usertype.nickname,
        avatar: usertype.avatar,
        phone: usertype.phone,
        mail: usertype.mail,
        gender: usertype.gender,
        sign: usertype.sign,
        birth: usertype.birth,
        ext: usertype.ext
    }
    let requestdata = url("",formdata)
    //console.log(requestdata)
    return getRequestDataStr({
        url: '/metadata/user/' + usernamestr,
        method: 'put',
        data: requestdata
    })
}

function param(data) {//把数组拼接url参数字符串
    let url = ''
    for (var k in data) {
        let value = data[k] !== undefined ? data[k] : ''
        url += '&' + k + '=' + encodeURIComponent(value)
    }
    //去除第一个&
    return url ? url.substring(1) : ''
}

function url(url,data){//数组拼接成url参数字符串
    url += (url.indexOf('?') < 0 ? '' : '&') + param(data)
    return url;
}
