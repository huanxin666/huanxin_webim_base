import axios from 'axios'
import {getToken} from '@/common/token'
const baseURL = 'http://a1.easemob.com/1138210906051608/demo'

export async function getRequest(formData) {//访问接口获取数据
	let isToken = getToken()
	let Authorization = ''
	if (isToken != '') {
		Authorization = 'Bearer ' + isToken
	}
	let requestdata = JSON.stringify(formData.data)
	return axios({//使用axios访问后端接口，获取数据
		method: formData.method,
		url: baseURL + formData.url,
		data: requestdata,
		headers: {
			'Content-Type': 'application/json',
			'Accept': 'application/json',
			'Authorization': Authorization
		},
		timeout: 10000
	}).then((res)=>{
		return res;//接口返回的数据
	}).catch(function (error) {
		console.log(error.response,'-------');//出错可以拿到后端返回的信息
		return error.response
	});
}


export async function getRequestDataStr(formData) {
	//访问接口获取数据
	let isToken = getToken()
	let Authorization = ''
	if (isToken != '') {
		Authorization = 'Bearer ' + isToken
	}
	let requestdata = formData.data
	return axios({//使用axios访问后端接口，获取数据
		method: formData.method,
		url: baseURL + formData.url,
		data: requestdata,
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Authorization': Authorization
		},
		timeout: 10000
	}).then((res)=>{
		return res;//接口返回的数据
	}).catch(function (error) {
		console.log(error.response,'-------');//出错可以拿到后端返回的信息
		return error.response
	});
}

